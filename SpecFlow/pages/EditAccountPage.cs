﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Interactions;

namespace SpecFlow.pages
{
    public class EditAccountPage : BaseHomePage
    {
        [FindsBy(How = How.Id, Using = "user_opting_in")]
        private IWebElement phoneMessagesSusbcribeCheckbox;

        [FindsBy(How = How.Id, Using = "subscription")]
        private IWebElement emailMessagesSusbcribeCheckbox;

        [FindsBy(How = How.XPath, Using = ".//*[@id='user_form']/div[2]/input")]
        private IWebElement saveAccountButton;

        public EditAccountPage(IWebDriver webDriver) : base(webDriver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void UpdateEmailSubscription(bool emailSubscriptionFlag)
        {

            bool isChecked = emailMessagesSusbcribeCheckbox.Selected;
            Actions action = new Actions(driver);
            action.MoveToElement(emailMessagesSusbcribeCheckbox).Perform();
            if (isChecked != emailSubscriptionFlag)
            {
                emailMessagesSusbcribeCheckbox.Click();
            }
        }
        public void UpdatePhoneSubscription(bool phoneSubscriptionFlag)
        {
            Actions action = new Actions(driver);
            action.MoveToElement(phoneMessagesSusbcribeCheckbox).Perform();
            bool isChecked = phoneMessagesSusbcribeCheckbox.Selected;
            if (isChecked != phoneSubscriptionFlag)
            {
                phoneMessagesSusbcribeCheckbox.Submit();
            }

        }

        public void Save()
        {
            Actions action = new Actions(driver);
            action.MoveToElement(saveAccountButton).Perform();
            saveAccountButton.Submit();

        }
    }
}
