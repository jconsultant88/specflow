﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace SpecFlow.pages
{
    public abstract class BaseHomePage : BasePage
    {
        [FindsBy(How = How.XPath, Using = ".//*[@id='main-navbar']/div/ul[2]/li[2]/div/ul/li[7]/a")]
        private IWebElement logoutLinkElement;

        [FindsBy(How = How.XPath, Using = ".//*[@id='main-navbar']/div/ul[2]/li[2]/div/a")]
        private IWebElement accountMenuDropdownElement;

        [FindsBy(How = How.XPath, Using = ".//*[@id='main-navbar']/div/ul[2]/li[2]/div/ul/li[2]/a")]
        private IWebElement editProfileLinkElement;

        public BaseHomePage(IWebDriver webDriver) : base(webDriver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void Logout()
        {
            AccountMenuDropDown();
            logoutLinkElement.Click();
        }

        public void AccountMenuDropDown()
        {
            accountMenuDropdownElement.Click();
            // TODO - Moved this to app.config
            Thread.Sleep(2000);
        }

        public void EditProfile()
        {
            editProfileLinkElement.Click();
        }
    }
}
