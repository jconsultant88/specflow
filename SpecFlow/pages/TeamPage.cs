﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SpecFlow.pages
{
    public class TeamPage : BaseHomePage
    {

        [FindsBy(How = How.CssSelector, Using = ".text-center.margin-top-small.margin-bottom-small.regular>b")]
        private IWebElement teamTextField;

        public TeamPage(IWebDriver webDriver) : base(webDriver)
        {
            PageFactory.InitElements(driver, this);
        }

        public string getTeamName()
        {

            return teamTextField.Text;
        }
    }
}
