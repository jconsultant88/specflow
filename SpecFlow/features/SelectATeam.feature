﻿Feature: Select a Team

@logout
Scenario: Login And Select Drive Party
    Given A consumer navigates to Teamer Welcome page
	And A consumer clicks in Login button
	And Consumer provides username and password in login page and clicks login button
	When Consumer clicks "Drive Party" Team
	Then consumer is shown "Drive Party" Team page

@logout
Scenario: Login And Select Shuttle Adhoc Team
    Given A consumer navigates to Teamer Welcome page
	And A consumer clicks in Login button
	And Consumer provides username and password in login page and clicks login button
	When Consumer clicks "Shuttle Adhoc Team" Team
	Then consumer is shown "Shuttle Adhoc Team" Team page