@echo off

@REM set the following locations appropriatly
set NDIR="C:\Users\IBM_ADMIN\Downloads\NUnit.Console-3.7.0\nunit3-console.exe"
set ADIR="C:\dev-workspaces\csharp\specflow\SpecFlow\bin\Debug"
set FILE_NAME="SpecFlow.exe"

set NUNIT_DIR=%NDIR%
IF EXIST "%NUNIT_DIR%" goto execute_project
echo.
echo Error: Nunit console is not detected in the system path
echo Please set the Nunit console variable
echo.
goto error

@REM ==== END VALIDATION ====

:execute_project
set APP_DIR=%ADIR%
IF EXIST "%APP_DIR%" goto execute_tests
echo Error: No such folder exists %APP_DIR%

:execute_tests
cd "%APP_DIR%"
echo executing tests...
%NUNIT_DIR% %FILE_NAME%

