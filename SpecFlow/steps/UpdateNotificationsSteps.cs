﻿using OpenQA.Selenium;
using SpecFlow.pages;
using TechTalk.SpecFlow;
namespace SpecFlow.steps
{
    [Binding]
    public class UpdateNotificationsSteps
    {
        private IWebDriver driver;
        private EditAccountPage editAccountPage;
        private const string emailCheckBoxLabel = "I want to receive Email messages";
        private const string phoneCheckBoxLabel = "I want to receive team event notices on my phone";

        public UpdateNotificationsSteps(IWebDriver webDriver)
        {
            this.driver = webDriver;
            editAccountPage = new EditAccountPage(driver);
        }

        [Given(@"Consumer clicks Edit Profile")]
        public void GivenConsumerClicksEditProfile()
        {
            editAccountPage.AccountMenuDropDown();
            editAccountPage.EditProfile();
        }

        [When(@"Consumer checks '(.*)' checkbox in edit account page")]
        public void WhenConsumerChecksEmailOrPhoneSubscription(string label)
        {
            if (string.Equals(label, emailCheckBoxLabel))
            {
                editAccountPage.UpdateEmailSubscription(true);
            }
            else if (string.Equals(label, phoneCheckBoxLabel))
            {
                editAccountPage.UpdatePhoneSubscription(true);
            }
            else
            {
                throw new NoSuchElementException(label);
            }
        }

        [When(@"Consumer unchecks '(.*)' checkbox in edit account page")]
        public void WhenConsumerUnChecksEmailOrPhoneSubscription(string label)
        {
            if (string.Equals(label, emailCheckBoxLabel))
            {
                editAccountPage.UpdateEmailSubscription(false);
            }
            else if (string.Equals(label, phoneCheckBoxLabel))
            {
                editAccountPage.UpdatePhoneSubscription(false);
            }
            else
            {
                throw new NoSuchElementException(label);
            }
        }

        [When(@"Consumer saves Edit Profile")]
        public void WhenConsumerSavesEditProfile()
        {
            editAccountPage.Save();
        }

        [Then(@"consumer is shown success message")]
        public void ThenConsumerIsShownSuccessMessage()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
