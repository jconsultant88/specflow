﻿using OpenQA.Selenium;
using SpecFlow.pages;
using System;
using System.Configuration;
using TechTalk.SpecFlow;

namespace SpecFlow.steps
{
    [Binding]
    class CommonSteps
    {
        private IWebDriver driver;
        private WelcomePage welcomePage;
        private DashBoardPage dashBoardPage;
        private TeamPage teamPage;

        public CommonSteps(IWebDriver webDriver)
        {
            this.driver = webDriver;
            welcomePage = new WelcomePage(driver);
            dashBoardPage = new DashBoardPage(driver);
            teamPage = new TeamPage(driver);
        }

        [Given(@"A consumer navigates to Teamer Welcome page")]
        public void GivenAConsumerNavigatesToTeamerWelcomePage()
        {
            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["URL"]);
        }

        [Given(@"A consumer clicks in Login button")]
        public void GivenAConsumerClicksInLoginButton()
        {
            welcomePage.SelectLogin(true);
        }

        [Given(@"Consumer provides username and password in login page and clicks login button")]
        public void GivenConsumerProvidesUsernameAndPasswordInLoginPageAndClicksLoginButton()
        {
            String email = ConfigurationManager.AppSettings["email"];
            String password = ConfigurationManager.AppSettings["password"];
            welcomePage.Login(email, password);
        }
    }
}
