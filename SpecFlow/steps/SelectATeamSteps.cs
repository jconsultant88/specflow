﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SpecFlow.pages;
using System;
using System.Configuration;
using TechTalk.SpecFlow;

namespace SpecFlow.steps
{
    [Binding]
    public class SelectATeamSteps
    {
        private IWebDriver driver;
        private DashBoardPage dashBoardPage;
        private TeamPage teamPage;

        public SelectATeamSteps(IWebDriver webDriver)
        {
            this.driver = webDriver;
            dashBoardPage = new DashBoardPage(driver);
            teamPage = new TeamPage(driver);
        }

        [When(@"Consumer clicks ""(.*)"" Team")]
        public void WhenConsumerClickTeam(string teamName)
        {
            dashBoardPage.ClickTeam(teamName);
        }

        [Then(@"consumer is shown ""(.*)"" Team page")]
        public void ThenConsumerIsShownTeamPage(string teamName)
        {
            Assert.IsTrue(string.Equals(teamName, teamPage.getTeamName()));
        }

        [AfterScenario]
        public void logout()
        {
            Console.WriteLine("Checking the tags");
            string[] scenarioTags = ScenarioContext.Current.ScenarioInfo.Tags;
            if (Array.IndexOf(scenarioTags, "logout") >= 0)
            {
                Console.WriteLine("Logging the user out");
                dashBoardPage.Logout();
            }
        }

    }
}
