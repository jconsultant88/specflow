﻿using BoDi;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace SpecFlow.context
{
    [Binding]
    public class SpecFlowWebDriverSupport
    {
        private readonly IObjectContainer objectContainer;
        private IWebDriver driver;
        public SpecFlowWebDriverSupport(IObjectContainer objectContainer)
        {
            this.objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void RunBeforeEachScenario()
        {
            driver = SeleniumDriverFactory.CreateNewInstance("chrome");
            objectContainer.RegisterInstanceAs<IWebDriver>(driver);
        }

        [AfterScenario]
        public void RunAfterEachScenarion()
        {
            if (!(driver == null))
            {
                Console.WriteLine("killing driver instance");
                driver.Close();
            }
            
            objectContainer.Dispose();
            Console.WriteLine("Driver instance is still not removed from factory");
            driver = null;
        }
    }

}
