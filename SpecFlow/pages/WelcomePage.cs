﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;

namespace SpecFlow.pages
{
    public class WelcomePage : BasePage
    {
        [FindsBy(How = How.XPath, Using = ".//*[@id='home']/div[3]/div/div/div[2]/div[3]/a")]
        private IWebElement loginElement;

        [FindsBy(How = How.Id, Using = "email")]
        private IWebElement emailInputField;

        [FindsBy(How = How.Id, Using = "password")]
        private IWebElement passwordInputField;

        [FindsBy(How = How.XPath, Using = ".//*[@id='login_form']/div[4]/button")]
        private IWebElement logInButton;

        public WelcomePage(IWebDriver webDriver) : base(webDriver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void SelectLogin(bool maximizeWindow)
        {
            loginElement.Click();

            if (maximizeWindow)
            {
                driver.Manage().Window.Maximize();
            }

        }

        public void Login(string email, string password)
        {
            emailInputField.SendKeys(email);
            passwordInputField.SendKeys(password);
            Console.WriteLine("email: " + email);
            Console.WriteLine("password: " + password);
            logInButton.Click();
        }
    }
}
