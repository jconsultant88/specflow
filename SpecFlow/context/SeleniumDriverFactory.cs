﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace SpecFlow.context
{
    public class SeleniumDriverFactory
    {
        private const string ie = "ie";
        private const string firefox = "firefox";
        private const string chrome = "chrome";
        public SeleniumDriverFactory()
        {
        }

        public static IWebDriver CreateNewInstance(String driverName)
        {
            //TODO : Refactor this code
            if (string.Equals(driverName, ie))
            {
                return new ChromeDriver();
            }
            else if (string.Equals(driverName, firefox))
            {
                return new ChromeDriver();
            }
            else if (string.Equals(driverName, chrome))
            {
                return new ChromeDriver();
            }
            else
            {
                return new ChromeDriver();
            }
        }
    }
}
