﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.ObjectModel;

namespace SpecFlow.pages
{
    public class DashBoardPage : BaseHomePage
    {

        public DashBoardPage(IWebDriver webDriver) : base(webDriver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void ClickTeam(string teamName)
        {
            ReadOnlyCollection<IWebElement> teamLinks = driver.FindElements(By.CssSelector(".link-it-all"));
            bool teamFound = false;
            foreach (IWebElement teamLink in teamLinks)
            {
                string text = teamLink.Text;
                if (string.Equals(teamName, text))
                {
                    teamLink.Click();
                    teamFound = true;
                    break;
                }
            }
            if (!teamFound)
            {
                throw new NoSuchElementException(string.Format("Team with name {0} not found ", teamName));
            }
        }
    }
}
