# SpecFlowTest

Project to learn spec flow

## Below steps are followed this project

 - Add spec flow from 'Extensions and Updates' and restart VS
 - Add Nunit
 - Add Nunit Driver 
 - Create a console app and add selenium web driver as reference{Nuget}
 - Copy ChromeDrive exe file in a location and from properties change 'copy to output directory' to 'copy if newer' 
 - Add as "Existing Item"  to the current project in Solution Explorer
 - Add Microsoft.VisualStudio.UnitTesting as Reference from Nuget

## Add Configuration support

Add the 'reference' to the assembly 'System.Configuration.dll' , by right-clicking on the 'References' tab, choose add reference and then find 'System.Configuration'.

## Visual Studio short cut keys
 - Ctrl + K, Ctrl + D to format your total file so it looks cleaner. 
 - CTRL + TAB -- moves you between open code pages
 - SHIFT + CTRL + F -- allows you to do a search across the entire solution
 - Ctrl + I - Incremental Search
 - Ctrl+K, Ctrl+C Comment a block
 - Ctrl+K, Ctrl+U Uncomment the block
 - Ctrl + R, Ctrl + G for oganising 'using'

## References

* [Getting Started Spec flow] - Gives an idea of how to start spec flow project
* [Read Config support in c sharp] - Gives an idea of how to start spec flow project
* [Share Driver instance] - Share web driver instance across classes

  [Getting Started Spec flow]: <https://www.agilemania.com/blog/acceptance-test-driven-development-atdd-example-gherkin-specflow-nunit-microsoft-platform-netc/>
  
  [Read Config support in c sharp]: <http://toolsqa.com/selenium-webdriver/c-sharp/manage-read-configurations-using-configurationmanager-csharp/>
  
  [Share Driver instance]: <https://stackoverflow.com/questions/26392380/nunit-specflow-how-to-share-a-class-instance-for-all-tests>
