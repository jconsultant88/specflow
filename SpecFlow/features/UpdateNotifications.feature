﻿Feature: UpdateNotifications

@logout
Scenario: Subscribes for Email notification
    Given A consumer navigates to Teamer Welcome page
	And A consumer clicks in Login button
	And Consumer provides username and password in login page and clicks login button
	And Consumer clicks Edit Profile
	When Consumer checks 'I want to receive Email messages' checkbox in edit account page
	And Consumer saves Edit Profile
	Then consumer is shown success message

@logout
Scenario: Subscribes for Phone notification
    Given A consumer navigates to Teamer Welcome page
	And A consumer clicks in Login button
	And Consumer provides username and password in login page and clicks login button
	And Consumer clicks Edit Profile
	When Consumer checks 'I want to receive team event notices on my phone' checkbox in edit account page
	And Consumer saves Edit Profile
	Then consumer is shown success message
